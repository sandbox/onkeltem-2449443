<?php

/**
 * Adds machine field names to 'help' to simplify search.
 * Borrows the code from views_fetch_fields().
 */
function views_fields_helper_field_views_data_alter(&$data) {
  static $strings = array();
  foreach ($data as $table => $table_data) {
    foreach ($table_data as $field => $info) {
      // Collect table data from this table
      if ($field == 'table') {
        continue;
      }
      foreach (array('field', 'sort', 'filter', 'argument', 'relationship', 'area') as $key) {
        if (!empty($info[$key])) {
          // Don't show old fields. The real field will be added right.
          if (isset($info[$key]['moved to'])) {
            continue;
          }
          foreach (array('help') as $string) {
            $help = t('Machine name') . ': ' . $field;
            $k1 = implode(':', array($table, $field, $key, $string));
            $k2 = implode(':', array($table, $field, $string));
            $k3 = implode(':', array($table, 'table', $string));
            // First, try the lowest possible level
            if (!empty($info[$key][$string]) && empty($strings[$k1])) {
              $data[$table][$field][$key][$string] .= ' ' . $help;
              $strings[$k1] = $help;
            }
            // Then try the field level
            elseif (!empty($info[$string]) && empty($strings[$k2])) {
              $data[$table][$field][$string] .= ' ' . $help;
              $strings[$k2] = $help;
            }
            // Finally, try the table level
            elseif (!empty($table_data['table'][$string]) && empty($strings[$k3])) {
              $data[$table]['table'][$string] .= ' ' . $help;
              $strings[$k3] = $help;
            }
          }
        }
      }
    }
  }
}

